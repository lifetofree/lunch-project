﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using libzkfpcsharp;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using Sample;
using Newtonsoft.Json;
using System.Configuration;

namespace Demo
{
    public partial class Form1 : Form
    {
        function_tool _funcTool = new function_tool();
        service_execute _serviceExec = new service_execute();
        data_lunch _data_lunch_template = new data_lunch();
        string _local_xml = "";

        IntPtr mDevHandle = IntPtr.Zero;
        IntPtr mDBHandle = IntPtr.Zero;
        IntPtr FormHandle = IntPtr.Zero;
        bool bIsTimeToDie = false;
        bool IsRegister = false;
        bool IsLoading = false;
        bool bIdentify = true;
        byte[] FPBuffer;
        int RegisterCount = 0;
        const int REGISTER_FINGER_COUNT = 3;

        byte[][] RegTmps = new byte[3][];
        byte[] RegTmp = new byte[2048];
        byte[] CapTmp = new byte[2048];
        int cbCapTmp = 2048;
        int cbRegTmp = 0;
        int iFid = 1;

        private int mfpWidth = 0;
        private int mfpHeight = 0;

        const int MESSAGE_CAPTURED_OK = 0x0400 + 6;

        [DllImport("user32.dll", EntryPoint = "SendMessageA")]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll")]
        public static extern bool Beep(int freq, int duration);

    public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FormHandle = this.Handle;
        }

        private void Form1_Shown(Object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (setInit())
            {
                if (setOpen())
                {
                    _data_lunch_template = getAllTemplate();
                    getFingerprintToMemory();
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void DoCapture()
        {
            while (!bIsTimeToDie)
            {
                cbCapTmp = 2048;
                int ret = zkfp2.AcquireFingerprint(mDevHandle, FPBuffer, CapTmp, ref cbCapTmp);
                if (ret == zkfp.ZKFP_ERR_OK)
                {
                    SendMessage(FormHandle, MESSAGE_CAPTURED_OK, IntPtr.Zero, IntPtr.Zero);
                }
                Thread.Sleep(200);
            }
        }

        protected override void DefWndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case MESSAGE_CAPTURED_OK:
                    MemoryStream ms = new MemoryStream();
                    BitmapFormat.GetBitmap(FPBuffer, mfpWidth, mfpHeight, ref ms);
                    Bitmap bmp = new Bitmap(ms);
                    this.picFPImg.Image = bmp;

                    String strShow = zkfp2.BlobToBase64(CapTmp, cbCapTmp);
                    //textRes.AppendText("capture template data:" + strShow + "\n");
                    tbTemplate.Text = strShow;

                    string emp_code = getMatchFingerprint(strShow);
                    setTransaction(emp_code);

                    //if (IsRegister)
                    //{
                    //    int ret = zkfp.ZKFP_ERR_OK;
                    //    int fid = 0, score = 0;
                    //    ret = zkfp2.DBIdentify(mDBHandle, CapTmp, ref fid, ref score);
                    //    if (zkfp.ZKFP_ERR_OK == ret)
                    //    {
                    //        textRes.Text = "This finger was already register by " + fid + "!";
                    //        return;
                    //    }
                    //    if (RegisterCount > 0 && zkfp2.DBMatch(mDBHandle, CapTmp, RegTmps[RegisterCount - 1]) <= 0)
                    //    {
                    //        textRes.Text = "Please press the same finger 3 times for the enrollment";
                    //        return;
                    //    }
                    //    Array.Copy(CapTmp, RegTmps[RegisterCount], cbCapTmp);
                    //    String strBase64 = zkfp2.BlobToBase64(CapTmp, cbCapTmp);
                    //    byte[] blob = zkfp2.Base64ToBlob(strBase64);
                    //    RegisterCount++;
                    //    if (RegisterCount >= REGISTER_FINGER_COUNT)
                    //    {
                    //        RegisterCount = 0;
                    //        if (zkfp.ZKFP_ERR_OK == (ret = zkfp2.DBMerge(mDBHandle, RegTmps[0], RegTmps[1], RegTmps[2], RegTmp, ref cbRegTmp)) &&
                    //               zkfp.ZKFP_ERR_OK == (ret = zkfp2.DBAdd(mDBHandle, iFid, RegTmp)))
                    //        {
                    //            iFid++;
                    //            textRes.Text = "enroll succ";
                    //        }
                    //        else
                    //        {
                    //            textRes.Text = "enroll fail, error code=" + ret;
                    //        }
                    //        IsRegister = false;
                    //        return;
                    //    }
                    //    else
                    //    {
                    //        textRes.Text = "You need to press the " + (REGISTER_FINGER_COUNT - RegisterCount) + " times fingerprint";
                    //    }
                    //}
                    //else
                    //{
                    //    if (cbRegTmp <= 0)
                    //    {
                    //        textRes.Text = "Please register your finger first!";
                    //        return;
                    //    }
                    //    if (bIdentify)
                    //    {
                    //        int ret = zkfp.ZKFP_ERR_OK;
                    //        int fid = 0, score = 0;
                    //        ret = zkfp2.DBIdentify(mDBHandle, CapTmp, ref fid, ref score);
                    //        if (zkfp.ZKFP_ERR_OK == ret)
                    //        {
                    //            textRes.Text = "Identify succ, fid= " + fid + ",score=" + score + "!";
                    //            return;
                    //        }
                    //        else
                    //        {
                    //            textRes.Text = "Identify fail, ret= " + ret;
                    //            return;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        int ret = zkfp2.DBMatch(mDBHandle, CapTmp, RegTmp);
                    //        if (0 < ret)
                    //        {
                    //            textRes.Text = "Match finger succ, score=" + ret + "!";
                    //            return;
                    //        }
                    //        else
                    //        {
                    //            textRes.Text = "Match finger fail, ret= " + ret;
                    //            return;
                    //        }
                    //}
                    //}
                    break;

                default:
                    base.DefWndProc(ref m);
                    break;
            }
        }

        private void bnInit_Click(object sender, EventArgs e)
        {
            setInit();
        }

        private void bnFree_Click(object sender, EventArgs e)
        {
            setFinalize();
        }

        private void bnOpen_Click(object sender, EventArgs e)
        {
            setOpen();
        }
        
        private void bnClose_Click(object sender, EventArgs e)
        {
            setClose();
        }

        private void bnEnroll_Click(object sender, EventArgs e)
        {
            if (!IsRegister)
            {
                IsRegister = true;
                RegisterCount = 0;
                cbRegTmp = 0;
                textRes.Text = "Please press your finger 3 times!";
            }
        }

        private void bnIdentify_Click(object sender, EventArgs e)
        {
            if (!bIdentify)
            {
                bIdentify = true;
                textRes.Text = "Please press your finger!";
            }
        }

        private void bnVerify_Click(object sender, EventArgs e)
        {
            if (bIdentify)
            {
                bIdentify = false;
                textRes.Text = "Please press your finger!";
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            getFingerprintToMemory();
        }

        private void btnMatch_Click(object sender, EventArgs e)
        {
            getMatchFingerprint(tbTemplate.Text);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            setClose();
            setFinalize();
            GC.SuppressFinalize(this);
            Application.Exit();
        }

        #region machine
        protected bool setInit()
        {
            cmbIdx.Items.Clear();
            int ret = zkfperrdef.ZKFP_ERR_OK;
            if ((ret = zkfp2.Init()) == zkfperrdef.ZKFP_ERR_OK)
            {
                int nCount = zkfp2.GetDeviceCount();
                if (nCount > 0)
                {
                    for (int i = 0; i < nCount; i++)
                    {
                        cmbIdx.Items.Add(i.ToString());
                    }
                    cmbIdx.SelectedIndex = 0;
                    bnInit.Enabled = false;
                    bnFree.Enabled = true;
                    bnOpen.Enabled = true;

                    return true;
                }
                else
                {
                    zkfp2.Terminate();
                    MessageBox.Show("No device connected!");

                    return false;
                }
            }
            else
            {
                MessageBox.Show("Initialize fail, ret=" + ret + " !");

                return false;
            }
        }

        protected bool setOpen()
        {
            int ret = zkfp.ZKFP_ERR_OK;
            if (IntPtr.Zero == (mDevHandle = zkfp2.OpenDevice(cmbIdx.SelectedIndex)))
            {
                MessageBox.Show("Open device fail");
                return false;
            }
            if (IntPtr.Zero == (mDBHandle = zkfp2.DBInit()))
            {
                MessageBox.Show("Init DB fail");
                zkfp2.CloseDevice(mDevHandle);
                mDevHandle = IntPtr.Zero;
                return false;
            }

            bnInit.Enabled = false;
            bnFree.Enabled = true;
            bnOpen.Enabled = false;
            bnClose.Enabled = true;
            bnEnroll.Enabled = true;
            bnVerify.Enabled = true;
            bnIdentify.Enabled = true;
            RegisterCount = 0;
            cbRegTmp = 0;
            iFid = 1;

            for (int i = 0; i < 3; i++)
            {
                RegTmps[i] = new byte[2048];
            }

            byte[] paramValue = new byte[4];
            int size = 4;
            zkfp2.GetParameters(mDevHandle, 1, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref mfpWidth);

            size = 4;
            zkfp2.GetParameters(mDevHandle, 2, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref mfpHeight);

            FPBuffer = new byte[mfpWidth * mfpHeight];

            Thread captureThread = new Thread(new ThreadStart(DoCapture));
            captureThread.IsBackground = true;
            captureThread.Start();
            bIsTimeToDie = false;
            //textRes.Text = "Open success" + Environment.NewLine;
            return true;
        }

        protected void setClose()
        {
            bIsTimeToDie = true;
            RegisterCount = 0;
            Thread.Sleep(1000);
            zkfp2.CloseDevice(mDevHandle);
            bnInit.Enabled = false;
            bnFree.Enabled = true;
            bnOpen.Enabled = true;
            bnClose.Enabled = false;
            bnEnroll.Enabled = false;
            bnVerify.Enabled = false;
            bnIdentify.Enabled = false;
        }

        protected void setFinalize()
        {
            zkfp2.DBFree(mDBHandle);
            zkfp2.Terminate();
            cbRegTmp = 0;
            bnInit.Enabled = true;
            bnFree.Enabled = false;
            bnOpen.Enabled = false;
            bnClose.Enabled = false;
            bnEnroll.Enabled = false;
            bnVerify.Enabled = false;
            bnIdentify.Enabled = false;
        }
        #endregion machine

        #region process
        protected data_lunch getAllTemplate()
        {
            lunch_meal_template_detail getTemplate = new lunch_meal_template_detail();
            _data_lunch_template.lunch_meal_template_list = new lunch_meal_template_detail[1];
            getTemplate.plant_idx = _funcTool.convertToInt(ConfigurationSettings.AppSettings["plant_idx"]); ;
            _data_lunch_template.lunch_meal_template_list[0] = getTemplate;

            _local_xml = _serviceExec.actionExec("conn_lunch", "data_lunch", "lunch_meal", _data_lunch_template, 12);

            _data_lunch_template = (data_lunch)_funcTool.convertXmlToObject(typeof(data_lunch), _local_xml);

            return _data_lunch_template;
        }

        protected void getFingerprintToMemory()
        {
            //textRes.Text += "Loading..." + Environment.NewLine;

            if (_data_lunch_template.lunch_meal_template_list != null)
            {
                var linq_load_template = (from t in _data_lunch_template.lunch_meal_template_list
                                          where t.fid == 0
                                          select t).Take(7000);

                foreach (lunch_meal_template_detail item in linq_load_template)
                //foreach (lunch_meal_template_detail item in _data_lunch_template.lunch_meal_template_list)
                {
                    if (item.template_index != "")
                    {
                        try
                        {
                            int ret = zkfp.ZKFP_ERR_OK;
                            int fid = 0, score = 0;
                            byte[] RegTmp = zkfp.Base64String2Blob(item.template_index);

                            //ret = zkfp2.DBIdentify(mDBHandle, RegTmp, ref fid, ref score); // check registered
                            //if (zkfp.ZKFP_ERR_OK != ret)
                            //{
                                if (zkfp.ZKFP_ERR_OK == (ret = zkfp2.DBAdd(mDBHandle, iFid, RegTmp)))
                                {
                                    item.fid = iFid;
                                    ////textRes.Text += "enroll success : " + item.emp_code + " | " + iFid.ToString() + Environment.NewLine;
                                    //textRes.AppendText("\n" + "enroll success : " + item.emp_code + " | " + iFid.ToString() + "\n");
                                    iFid++;
                                }
                            //}
                            //else
                            //{
                            //textRes.Text += "This finger was already register by " + fid + "!" + Environment.NewLine;
                            //}
                        }
                        catch (Exception ex)
                        {
                            //textRes.AppendText("\n" + "Error = " + ex.Message + "\n");
                        }
                        finally
                        {
                            GC.SuppressFinalize(this);
                            //GC.Collect();
                        }
                    }
                    else
                    {
                        item.fid = -1;
                    }
                }
                textRes.Text += "Ready..." + Environment.NewLine;
                setLastPosition();
            }
            else
            {
                textRes.Text += Environment.NewLine + "Source is null" + Environment.NewLine;
            }

            setLastPosition();
        }

        protected string getMatchFingerprint(string sTemplate)
        {
            int ret = zkfp.ZKFP_ERR_OK;
            int fid = 0, score = 0;
            byte[] CapTmp = zkfp.Base64String2Blob(sTemplate);
            ret = zkfp2.DBIdentify(mDBHandle, CapTmp, ref fid, ref score);

            if (zkfp.ZKFP_ERR_OK == ret && score >= 60)
            {
                var linq_check_template = from t in _data_lunch_template.lunch_meal_template_list
                                            where t.template_index != "" && t.fid == fid
                                            select t;
                string _emp = "";
                foreach (lunch_meal_template_detail item in linq_check_template)
                {
                    _emp = item.emp_code;
                }

                //textRes.Text += "Identify succ, fid= " + fid + ",score=" + score + "!" + " emp_code = " + _emp + Environment.NewLine;
                return _emp;
            }
            else
            {
                //textRes.Text += "Identify fail, ret= " + ret + Environment.NewLine;
                return "0";
            }
        }

        protected void setTransaction(string _emp_code)
        {
            if (_emp_code != "0" && _emp_code != "")
            {
                data_lunch _check_trans = new data_lunch();
                lunch_meal_detail _lunch_detail = new lunch_meal_detail();
                _lunch_detail.emp_code = _emp_code;
                _lunch_detail.res_idx = _funcTool.convertToInt(ConfigurationSettings.AppSettings["res_idx"]);
                _lunch_detail.interval_min = _funcTool.convertToInt(ConfigurationSettings.AppSettings["interval_min"]);
                _check_trans.lunch_meal_list = new lunch_meal_detail[1];
                _check_trans.lunch_meal_list[0] = _lunch_detail;

                // production
                _local_xml = _serviceExec.actionExec("conn_lunch", "data_lunch", "lunch_meal", _check_trans, 11);
                _check_trans = (data_lunch)_funcTool.convertXmlToObject(typeof(data_lunch), _local_xml);
                // production

                // test
                //_check_trans.return_code = 0;
                //_check_trans.return_msg = "success";
                // test

                if (_check_trans.return_code == 0 && _check_trans.return_msg == "success")
                {
                    textRes.Text += DateTime.Now.ToString("HH:mm:ss") + " : " + _emp_code + " : Pass" + Environment.NewLine;
                    picLed.Image = Properties.Resources.img_led_green;
                    picLed.Refresh();
                    Beep(2000, 400);
                }
                else
                {
                    textRes.Text += DateTime.Now.ToString("HH:mm:ss") + " : " + _emp_code + " : Not pass" + Environment.NewLine;
                    picLed.Image = Properties.Resources.img_led_red;
                    picLed.Refresh();
                }
            }
            else
            {
                textRes.Text += DateTime.Now.ToString("HH:mm:ss") + " : " + "Data not found" + Environment.NewLine;
                picLed.Image = Properties.Resources.img_led_gray;
                picLed.Refresh();
            }

            setLastPosition();
        }
        #endregion process

        #region others
        private void setLastPosition()
        {
            textRes.SelectionStart = textRes.Text.Length - 1;// + 1;
            //textRes.SelectionLength = 0;
            textRes.ScrollToCaret();
        }
        #endregion others
    }
}
