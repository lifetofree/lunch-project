﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Demo
{
    class function_tool
    {
        #region convert custom array object
        public string convertObjectToXml(Object objData)
        {
            try
            {
                var xmlDoc = new XmlDocument(); //Represents an XML document, 
                                                // Initializes a new instance of the XmlDocument class.          
                var xmlSerializer = new XmlSerializer(objData.GetType());
                // Create empty namespace
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);
                // Creates a stream whose backing store is memory. 
                using (var xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, objData, namespaces);
                    xmlStream.Position = 0;
                    //Loads the XML document from the specified string.
                    xmlDoc.Load(xmlStream);
                    foreach (XmlNode node in xmlDoc)
                    {
                        if (node.NodeType == XmlNodeType.XmlDeclaration)
                        {
                            xmlDoc.RemoveChild(node);
                        }
                    }
                    return xmlDoc.InnerXml;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public Object convertXmlToObject(Type dataName, string xmlText)
        {
            try
            {
                var deserializer = new XmlSerializer(dataName);
                TextReader reader = new StringReader(xmlText);
                Object retData = deserializer.Deserialize(reader);
                reader.Close();

                return retData;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region XML to JSON and JSON to XML
        public string convertXmlToJson(string dataXml)
        {
            try
            {
                var doc = new XmlDocument();
                doc.LoadXml(dataXml);
                return JsonConvert.SerializeXmlNode(doc);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string convertJsonToXml(string dataJson)
        {
            try
            {
                XNode node = JsonConvert.DeserializeXNode(dataJson);
                return node.ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Json to Object and Object to Json
        public Object convertJsonToObject(Type dataName, string dataJson)
        {
            try
            {
                // convert Json to Xml
                string _localJson = convertJsonToXml(dataJson);
                // convert Xml to Object
                return convertXmlToObject(dataName, _localJson);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string convertObjectToJson(Object objData)
        {
            try
            {
                // convert Object to Xml
                string _localXml = convertObjectToXml(objData);
                // convert Xml to Json
                return convertXmlToJson(_localXml);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion Json to Object and Object to Json

        public int convertToInt(string dataIn)
        {
            int _default_int = 0;
            return int.TryParse(dataIn, out _default_int) ? int.Parse(dataIn) : _default_int;
        }
    }
}
