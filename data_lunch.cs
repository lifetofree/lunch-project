﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Demo
{
    [Serializable]
    [XmlRoot("data_lunch")]
    public class data_lunch
    {
        [XmlElement("return_code")]
        public int return_code { get; set; }
        [XmlElement("return_msg")]
        public string return_msg { get; set; }
        [XmlElement("return_idx")]
        public int return_idx { get; set; }

        [XmlElement("lunch_meal_list")]
        public lunch_meal_detail[] lunch_meal_list { get; set; }
        [XmlElement("lunch_meal_template_list")]
        public lunch_meal_template_detail[] lunch_meal_template_list { get; set; }
    }

    [Serializable]
    public class lunch_meal_detail
    {
        [XmlElement("u0_idx")]
        public int u0_idx { get; set; }
        [XmlElement("emp_idx")]
        public int emp_idx { get; set; }
        [XmlElement("emp_code")]
        public string emp_code { get; set; }
        [XmlElement("res_idx")]
        public int res_idx { get; set; }
        [XmlElement("interval_min")]
        public int interval_min { get; set; }
    }

    [Serializable]
    public class lunch_meal_template_detail
    {
        [XmlElement("emp_idx")]
        public int emp_idx { get; set; }
        [XmlElement("emp_code")]
        public string emp_code { get; set; }
        [XmlElement("template_index")]
        public string template_index { get; set; }
        [XmlElement("plant_idx")]
        public int plant_idx { get; set; }
        [XmlElement("fid")]
        public int fid { get; set; }
    }
}
