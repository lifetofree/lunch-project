﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demo
{
    class service_execute
    {
        #region initial function/data
        function_db _funcDb = new function_db();
        function_tool _funcTool = new function_tool();

        string _spName = String.Empty;
        string _localXml = String.Empty;
        string _localJson = String.Empty;
        string _resultCode = String.Empty;
        string _resultMsg = String.Empty;

        #endregion  initial function/data

        public string actionExec(string connName, string dataName, string cmdName, Object objIn, int actionType)
        {
            // switch stored procedure name
            switch (cmdName)
            {
                case "lunch_meal":
                    _spName = "sp_lm_finger_lunch";
                    break;
                default:
                    _spName = null;
                    break;
            }

            // exec  stored procedure
            if (_spName != null)
            {
                // convert to xml
                _localXml = _funcTool.convertObjectToXml(objIn);
                // execute
                _localXml = _funcDb.execSPXml(connName, _spName, _localXml, actionType);
            }
            else
            {
                // set for null _spName
                _resultCode = "\"return_code\": 000";
                _resultMsg = "\"return_msg\": \"stored procedure is null\"";

                _localJson = "{\"" + dataName + "\": " + "{" + _resultCode + "," + _resultMsg + "}";
                // convert to xml
                _localXml = _funcTool.convertJsonToXml(_localJson);
            }

            return _localXml;
        }
    }
}
