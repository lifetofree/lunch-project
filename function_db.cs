﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Demo
{
    class function_db
    {
        public SqlConnection strDBConn(string connName)
        {
            string db_ip = ConfigurationSettings.AppSettings["db_ip"];
            string db_user = ConfigurationSettings.AppSettings["db_user"];
            string db_password = ConfigurationSettings.AppSettings["db_password"];

            string strConn = "Data Source=" + db_ip + ";Initial Catalog=lunch_meal;Persist Security Info=True;User ID=" + db_user + ";Password=" + db_password;
            //"Data Source=localhost;Initial Catalog=lunch_meal;Persist Security Info=True;User ID=webconn;Password=Y$PP9Z6hCw";
            //ConfigurationManager.ConnectionStrings["connName"].ConnectionString;
            SqlConnection retDBConn = new SqlConnection(strConn);

            return retDBConn;
        }

        public string execSPXml(string connName, string spName, string xmlIn, int actionType)
        {
            var retXml = "";

            SqlConnection conn = strDBConn(connName);
            SqlCommand cmd = new SqlCommand(spName, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@xml_in", SqlDbType.Xml).Value = xmlIn;
            cmd.Parameters.Add("@action_type", SqlDbType.Int).Value = actionType;
            cmd.Parameters.Add("@xml_out", SqlDbType.Xml, 500000).Direction = ParameterDirection.Output;

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                retXml = cmd.Parameters["@xml_out"].Value.ToString();
            }
            catch (Exception ex)
            {
                retXml = ex.Message;
            }
            finally
            {
                conn.Close();
            }

            return retXml.ToString();
        }
    }
}
